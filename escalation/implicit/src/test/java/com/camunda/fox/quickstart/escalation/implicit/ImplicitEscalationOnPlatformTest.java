package com.camunda.fox.quickstart.escalation.implicit;

import javax.inject.Inject;
import org.activiti.engine.ProcessEngine;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;

import org.junit.runner.RunWith;

/**
 * Test case running on the fox platform via Arquillian.
 * Requires a running fox-platform.
 * 
 * @author nico.rehwaldt
 */
@RunWith(Arquillian.class)
public class ImplicitEscalationOnPlatformTest extends ImplicitEscalationTestBase {
  
  @Inject
  private ProcessEngine engine;
  
  @Deployment
  public static Archive<?> createDeployment() {
    
    MavenDependencyResolver resolver = DependencyResolvers
      .use(MavenDependencyResolver.class)
        .loadMetadataFromPom("pom.xml");
    
    // if you experience problems with the authentication to the camunda fox
    // repository the wrong maven configuration might be used.
    // use this code to usee your maven settings.xml in this case:
    // .configureFrom(".../settings.xml")

    Archive<?> archive = ShrinkWrap
      .create(WebArchive.class, "escalation-implicit.war")
        // prepare as process application archive for fox platform
        .addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAs(JavaArchive.class))
        .addAsWebResource("META-INF/test-processes.xml", "WEB-INF/classes/META-INF/processes.xml")
        .addAsWebResource(EmptyAsset.INSTANCE, "WEB-INF/beans.xml")
        // add your own classes (could be done one by one as well)
        .addPackages(false, "com.camunda.fox.quickstart.escalation.implicit")
        // add process definition
        .addAsResource("escalation-implicit.bpmn");
    
    return archive;
  }
  
  @Test
  public void shouldParseExtension() {
    // Implicitly done as the archive is deployed as a process engine
  }
  
  @Test
  public void shouldAssignDueDateToTaskWithExtension() {
    shouldHandleEscalationInProcessCorrectly(engine);
  }
}
