package com.camunda.fox.quickstart.escalation.implicit;

import java.util.Date;
import java.util.List;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import static org.junit.Assert.*;

/**
 *
 * @author nico.rehwaldt
 */
public class ImplicitEscalationTestBase {

  public void shouldHandleEscalationInProcessCorrectly(ProcessEngine engine) {
    
    ProcessInstance pi = engine.getRuntimeService().startProcessInstanceByKey("escalation-implicit");
    
    List<Task> tasks = engine.getTaskService().createTaskQuery().processInstanceId(pi.getId()).list();
    assertEquals(1, tasks.size());
    
    Task t = tasks.get(0);
    Date dueDate = t.getDueDate();
    
    assertNotNull(dueDate); 
    // We set duration to PT1M (one minute) 
    // lets check if the due date is actualy less than one minute away
    
    Date later = new Date(System.currentTimeMillis() + 1 * 60 * 1000);

    assertTrue(dueDate.before(later));
    
    // Get due tasks
    List<Task> dueTasks = engine.getTaskService().createTaskQuery().processInstanceId(pi.getId()).dueBefore(later).list();
    assertEquals(1, dueTasks.size());
  }
}
