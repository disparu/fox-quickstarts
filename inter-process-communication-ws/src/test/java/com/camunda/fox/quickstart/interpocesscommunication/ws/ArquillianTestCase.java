package com.camunda.fox.quickstart.interpocesscommunication.ws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.io.FileUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArquillianTestCase {

  @Deployment  
  public static WebArchive createDeployment() {
    MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom("pom.xml");
    
    return ShrinkWrap.create(WebArchive.class, "inter-process-communication-ws.war")
            // prepare as process application archive for fox platform
//            .addAsManifestResource("ARQUILLIAN-MANIFEST-JBOSS7.MF", "MANIFEST.MF")
    		.addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAs(JavaArchive.class))
            .addAsWebResource("META-INF/processes.xml", "WEB-INF/classes/META-INF/processes.xml")
            // add your own classes (could be done one by one as well)
            .addPackages(true, "com.camunda.fox.quickstart.interpocesscommunication.ws")
            // add process definition
            .addAsResource("parent.bpmn")
            .addAsResource("child.bpmn")
            // now you can add additional stuff required for your test case
            .addAsWebInfResource(new File("src/main/webapp", "WEB-INF/beans.xml"), "beans.xml")   
            .addAsLibraries(resolver.artifact("commons-io:commons-io").resolveAsFiles())
            ;    
  }

  @Inject
  private RuntimeService runtimeService;

  @Inject
  private TaskService taskService;
  
  @Test
  public void test() {
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("inter-process-communication-ws-parent");
    String pid = processInstance.getId();
    String correlationId = (String) runtimeService
            .getVariable(pid, ProcessInvocationClient.CORRELATION_ID_PREFIX + "inter-process-communication-ws-child");
    
    Task task = taskService
            .createTaskQuery()
            .processDefinitionKey("inter-process-communication-ws-child")
            .processVariableValueEquals(ProcessInvocation.CALLBACK_CORRELATION_ID, correlationId)
            .singleResult();
    assertNotNull(task);
    assertEquals("Child Process Called", task.getName());
    taskService.complete(task.getId());
    
    task = taskService.createTaskQuery().processInstanceId(pid).singleResult();
    assertNotNull(task);
    assertEquals("Callback reveived", task.getName());
    
    assertPayloadTransmittedCorrectly(pid, correlationId);
    
    taskService.complete(task.getId());
  }

  private void assertPayloadTransmittedCorrectly(String pid, String correlationId) {
    assertEquals(
            ProcessInvocationClient.SAMPLE_PAYLOAD_PREFIX + correlationId,
            runtimeService.getVariable(pid, ProcessCallback.PAYLOAD_RECEIVED_FROM_CALLBACK));
  }
  
  @RunAsClient
  @Test
  public void downloadWSDL() throws IOException {
    ServiceRegistry serviceRegistry = new ServiceRegistry();
    serviceRegistry.init();
    for (Entry<String, String> entry : serviceRegistry .getUrls().entrySet()) {
      String url = entry.getValue();
      System.out.println("Donwloading WSDL from " + url);
      String fileName = url.replace("http://localhost:19090/inter-process-communication-ws/", "").replace("?", "Service.");
      FileUtils.copyURLToFile(new URL(url), new File("src/main/resources/" + fileName));
    }
  }
}
