package com.camunda.fox.quickstart.escalation.implicit;

import com.camunda.fox.quickstart.escalation.implicit.extension.ExtensionEscalationTaskListener;
import java.io.InputStream;
import org.activiti.cdi.impl.util.ProgrammaticBeanLookup;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.calendar.DurationHelper;
import org.activiti.engine.impl.cmd.GetDeploymentProcessModelCmd;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.w3c.dom.Element;

/**
 * An escalation task listener which does not require an engine extension to work
 * 
 * See {@link ExtensionEscalationTaskListener} for a task listener which performs slightly faster but does require
 * to extend the engine.
 * 
 * @author nico.rehwaldt
 */
public class EscalationTaskListener implements TaskListener {

  @Override
  public void notify(DelegateTask delegateTask) {
    TaskEntity task = (TaskEntity) delegateTask;
    
    DurationHelper helper = parseEscalationDuration(task);
    if (helper != null) {
      task.setDueDate(helper.getDateAfter());
    }
  }
  
  protected DurationHelper parseEscalationDuration(TaskEntity task) {
    // Without we fall back to XML parsing:
    InputStream processModelInputStream = getProcessModel(task.getProcessDefinitionId());
    Element escalateAfterElement = Helper.getUserTaskExtensions(processModelInputStream, task.getExecution().getActivityId(), Helper.ESCALATE_AFTER_ELEMENT_NAME);
    
    if (escalateAfterElement != null) {
      return Helper.parseEscalateAfterDuration(escalateAfterElement);
    } else {
      return null;
    }
  }
  
  protected InputStream getProcessModel(String processDefinitionId) {
    
    // Executing RepositoryService#getProcessModel(task.getProcessDefinitionId())
    Command<InputStream> command = new GetDeploymentProcessModelCmd(processDefinitionId);
    return command.execute(Context.getCommandContext());
  }
}
