package com.camunda.fox.quickstart.escalation.explicit;

import java.util.List;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import static org.junit.Assert.*;

/**
 *
 * @author nico.rehwaldt
 */
public class ExplicitEscalationTestBase {

  public void shouldHandleEscalationCorrectly(ProcessEngine engine) throws Exception {
    
    ProcessInstance pi = engine.getRuntimeService().startProcessInstanceByKey("escalation-explicit");
    
    List<Task> tasks = engine.getTaskService().createTaskQuery().processInstanceId(pi.getId()).list();
    
    // task exists
    assertEquals(1, tasks.size());
    
    Task task = tasks.get(0);
    // right task?
    assertEquals("Categorize Invoice", task.getName());
    
    // No due date set
    assertNull(task.getDueDate());
    
    // see if escalation timer is triggered (we configured PT10S - 10 seconds)
    // sleep 10 seconds and check what happens
    Thread.sleep(15 * 1000);
    
    List<Task> newTasks = engine.getTaskService().createTaskQuery().processInstanceId(pi.getId()).list();
    
    // we got one task once again
    assertEquals(1, newTasks.size());
    
    Task newTask = newTasks.get(0);
    
    // but it is a different one this time (our escalation task)
    assertEquals("Check overdue categorization of invoice", newTask.getName());
  }
}
