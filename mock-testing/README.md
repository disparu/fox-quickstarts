# Introduction
This quick-start shows the usage of activiti's mock utility-classes. It contains a one-task process that calls a bean from a 
BPMN service task. The bean is mocked so there is no need to boot up a container.

# Motivation
The idea is that you should be able to test the process definition independently of the container or 
dependency injection framework you use (i.e. Spring or CDI).

# Running the quick-start
Just open a terminal window and type "mvn test" in the root directory of this quick-start. Alternatively you can import the
maven project into your favorite IDE and run the test cases there.

# Further Documentation
For more infos, have a look at the "Mock Testing" article in the camunda fox user guide at 
https://app.camunda.com/confluence/display/foxUserGuide/Mock+Testing

Have fun!