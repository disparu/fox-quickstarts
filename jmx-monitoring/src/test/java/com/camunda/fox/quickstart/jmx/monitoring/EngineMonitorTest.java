package com.camunda.fox.quickstart.jmx.monitoring;

import junit.framework.Assert;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 *
 * @author nico.rehwaldt
 */
public class EngineMonitorTest {

  private static ProcessEngine processEngine;
  
  @BeforeClass
  public static void beforeClass() {
    
    ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) 
      ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration()
        .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
        .setJdbcDriver("org.h2.Driver")
        .setJdbcUrl("jdbc:h2:mem:test;MVCC=true;DB_CLOSE_DELAY=-1")
        .setJdbcUsername("sa");
    
    processEngine = processEngineConfiguration.buildProcessEngine();
  }
  
  @AfterClass
  public static void afterClass() {
    processEngine.close();
  }
  
  @Test
  public void testGetFailedJobs() throws Exception {
    EngineMonitor engineMonitor = new EngineMonitor(processEngine);
    
    long time = engineMonitor.getFailedJobsCount();
    Assert.assertEquals(time, 0);
  }
  
  @Test
  public void testGetRunningProcessInstancesCount() throws Exception {
    EngineMonitor engineMonitor = new EngineMonitor(processEngine);
    
    long time = engineMonitor.getRunningProcessInstanceCount();
    Assert.assertEquals(time, 0);
  }
  
  @Test
  public void testGetProcessInstancesProcessingTimeLastDay() throws Exception {
    EngineMonitor engineMonitor = new EngineMonitor(processEngine);
    
    long time = engineMonitor.getProcessInstancesProcessingTimeLastDay();
    Assert.assertEquals(time, -1);
  }
}
