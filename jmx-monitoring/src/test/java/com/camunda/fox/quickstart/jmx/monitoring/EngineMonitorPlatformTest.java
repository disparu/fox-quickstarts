package com.camunda.fox.quickstart.jmx.monitoring;

import java.lang.management.ManagementFactory;
import javax.inject.Inject;
import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.Query;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 *
 * @author nico.rehwaldt
 */
@RunWith(Arquillian.class)
public class EngineMonitorPlatformTest {

  @Deployment
  public static Archive<?> createApplicationDeployment() {
    return ShrinkWrap
      .create(WebArchive.class)
        .addPackage("com.camunda.fox.quickstart.jmx.monitoring")
        .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
  }
  
  @Inject
  private MonitorRegistration monitorRegistration;
  
  @Test
  public void testJMXMonitorRegistrationOnPlatform() {
    
    EngineMonitor monitor = monitorRegistration.getMonitor();
    assertNotNull(monitor);
    
    // should be able to execute queries
    monitor.getRunningProcessInstanceCount();
    monitor.getEngineName();
    monitor.getFailedJobsCount();
    monitor.getRunningProcessInstanceCount();
  }
  
  @Test
  public void testJMXMonitoringOnPlatform() throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName monitoringBeanName = new ObjectName("com.camunda.fox.platform.jmx" + ":type=" + "EngineMonitorMXBean,engineName=default");
    
    // Make sure mbean is registered
    ObjectInstance objectInstance = mbs.getObjectInstance(monitoringBeanName);
    assertEquals(objectInstance.getClassName(), EngineMonitor.class.getName());
  }
}
