package com.camunda.fox.quickstart.test.mock;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.activiti.engine.test.mock.Mocks;
import org.junit.Rule;
import org.junit.Test;

import com.camunda.fox.quickstart.bean.HelloBean;

public class JUnit4ProcessTest {

  @Rule
  public ActivitiRule activitiRule = new ActivitiRule();

  @Test
  @Deployment(resources = "process.bpmn")
  public void testHappyPath() throws InterruptedException {
    // Initialize mocks
    HelloBean helloBean = mock(HelloBean.class);
    when(helloBean.sayHello("John")).thenReturn("Hello John!");
    // Register mocks
    Mocks.register("helloBean", helloBean);

    // Start process instance with variables
    HashMap<String, Object> variables = new HashMap<String, Object>();
    variables.put("name", "John");
    RuntimeService runtimeService = activitiRule.getRuntimeService();
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("hello_process", variables);
    
    // Print the ID of the process instance that has been started to the console
    System.out.println("Started process instance id " + processInstance.getId());

    // un-comment the code below if you make the service task in hellp_process asynchronous!
    
//    // assert the asynchronous flag was set on the service task
//    ManagementService managementService = activitiRule.getManagementService();
//    Job job = managementService.createJobQuery().singleResult();
//    assertNotNull(job);
//  
//    // manually execute the job
//    managementService.executeJob(job.getId());
    
    // Assert process ended
    HistoryService historyService = activitiRule.getHistoryService();
    HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstance.getId()).singleResult();
    assertNotNull(historicProcessInstance.getEndActivityId());

    // verify the service was called exactly one time with the parameter "John"
    verify(helloBean, times(1)).sayHello("John");

    // reset the mocks to make sure we have a clean start in the next test case
    Mocks.reset();
  }
}
